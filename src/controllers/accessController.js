'use strict';

const AccessService = require("../services/accessServices");

class AccessController {
    signUp = async (req, res, next) => {
        try {
            console.log(`[P]:::SignUp:::`, req.body)
            return res.status(201).json(await AccessService.signUp(req.body))
        } catch (error) {
            console.log('error:', error)
            next(error);
        }
    }
}

module.exports = new AccessController();